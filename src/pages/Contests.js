/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import gql from 'graphql-tag'
import { Query, Mutation } from 'react-apollo'
import { Row, Col, Divider, Table, Popconfirm, Button, message, Spin } from 'antd'
import { withStateHandlers } from 'recompose'
import { filter } from 'lodash'

const CONSTESTS_QUERY = gql`
  query Contests(
    $sortOrder: SortOrderEnum
    $sortField: ContestsSortFieldEnum
    $filters: ContestsFilterFields
    $limit: Int = 4
    $skip: Int
  ) {
    contests(
      sortField: $sortField
      sortOrder: $sortOrder
      filters: $filters
      limit: $limit
      skip: $skip
    ) {
      count
      items {
        _id
        title
        createdAt(format: "DD.MM.YYYY, HH:mm")
        startDate(format: "DD.MM.YYYY, HH:mm")
        endDate(format: "DD.MM.YYYY, HH:mm")
        socialNetwork
        type
        author {
          _id
          name
        }
      }
    }
  }
`

const REMOVE_CONTEST_MUTATION = gql`
  mutation RemoveContest($_id: ID!) {
    removeContest(_id: $_id) {
      _id
      title
    }
  }
`

const Contests = ({ variables, setVariables }) =>
  <React.Fragment>
    <Row>
      <Col><h1>Конкурсы</h1></Col>
    </Row>
    <Divider />
    <Row style={{ marginTop: '8px' }}>
      <Query
        query={CONSTESTS_QUERY}
        fetchPolicy='cache-and-network'
        variables={variables}
      >
        {({ data, loading, fetchMore }) => {
          if (loading && !data.contests) {
            return <Spin />
          }
          return (
            <React.Fragment>
              <Table
                bordered
                loading={loading}
                onChange={setVariables}
                rowKey='_id'
                dataSource={data.contests.items}
                pagination={false}
                columns={[
                  {
                    title: 'Название',
                    sorter: true,
                    key: 'title',
                    dataIndex: 'title'
                  },
                  {
                    title: 'Тип конкурса',
                    filters: [
                      { text: 'Случайный', value: 'random' },
                      { text: 'Лайк', value: 'like' },
                      { text: 'Репост', value: 'repost' },
                      { text: 'Коммент', value: 'comment' }
                    ],
                    key: 'type',
                    dataIndex: 'type'
                  },
                  {
                    title: 'Социальная сеть',
                    filters: [
                      { text: 'Twitter', value: 'twitter' },
                      { text: 'Instagram', value: 'instagram' }
                    ],
                    key: 'socialNetwork',
                    dataIndex: 'socialNetwork'
                  },
                  {
                    title: 'Автор',
                    key: 'author',
                    dataIndex: 'author.name'
                  },
                  {
                    title: 'Создано',
                    sorter: true,
                    key: 'createdAt',
                    dataIndex: 'createdAt'
                  },
                  {
                    title: 'Начало конкурса',
                    key: 'startDate',
                    dataIndex: 'startDate'
                  },
                  {
                    title: 'Конец конкурса',
                    sorter: true,
                    key: 'endDate',
                    dataIndex: 'endDate'
                  },
                  {
                    title: '',
                    key: 'x',
                    width: 40,
                    dataIndex: '',
                    render: (text, { title, _id }) =>
                      <Mutation
                        mutation={REMOVE_CONTEST_MUTATION}
                        variables={{ _id }}
                        onError={(error) => message.error(error.message)}
                        update={(store, { data: { removeContest } }) => {
                          /**
                           * Get current store from query
                           */
                          const data = store.readQuery({ query: CONSTESTS_QUERY, variables })

                          /**
                           * Remove deleted contest from locale store
                           */
                          data.contests.count -= 1
                          data.contests.items = filter(data.contests.items, (contest) =>
                            contest._id !== removeContest._id
                          )

                          /**
                           * Write new data wihtout deleted contest
                           */
                          store.writeQuery({ query: CONSTESTS_QUERY, data, variables })

                          /**
                           * Show success message
                           */
                          message.success(`Конкурс "${removeContest.title}" был удален`)
                        }}
                      >
                        {(removeContest) =>
                          <Popconfirm
                            placement='left'
                            title={`Вы действительно хотите удалить конкурс ${title}?`}
                            onConfirm={removeContest}
                            okText='Да'
                            cancelText='Нет'
                          >
                            <Button type='danger' icon='delete' />
                          </Popconfirm>
                        }
                      </Mutation>
                  }
                ]}
              />
              {data.contests.count > data.contests.items.length &&
                <Button
                  style={{ marginTop: 10 }}
                  icon='plus'
                  onClick={() =>
                    fetchMore({
                      variables: {
                        ...variables,
                        skip: data.contests.items.length
                      },
                      updateQuery: (prev, { fetchMoreResult }) => {
                        if (!fetchMoreResult) {
                          return prev
                        }
                        return {
                          ...prev,
                          contests: {
                            ...prev.contests,
                            items: [
                              ...prev.contests.items,
                              ...fetchMoreResult.contests.items
                            ]
                          }
                        }
                      }
                    })
                  }
                >
                  Загрузить еще
                </Button>
              }
            </React.Fragment>
          )
        }}
      </Query>
    </Row>
  </React.Fragment>

Contests.propTypes = {
  variables: PropTypes.object.isRequired,
  setVariables: PropTypes.func.isRequired
}

const enhance = withStateHandlers(
  {
    variables: {}
  },
  {
    setVariables: ({ variables }) => (pagination, filters, sorter) => ({
      variables: {
        ...variables,
        filters,
        sortField: sorter.field,
        sortOrder: sorter.order
      }
    })
  }
)

export default enhance(Contests)
