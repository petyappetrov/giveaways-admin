/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import gql from 'graphql-tag'
import localforage from 'localforage'
import { Redirect } from 'react-router-dom'
import { withApollo, graphql } from 'react-apollo'
import { compose, withHandlers } from 'recompose'
import { forEach } from 'lodash'
import { Layout, Form, Icon, Input, Button, Row, Col, message } from 'antd'

const LOGIN_MUTATE = gql`
  mutation Login(
    $email: String!
    $password: String!
  ) {
    loginAdmin(
      email: $email
      password: $password
    ) {
      jwt
    }
  }
`

const Login = ({ me, client, form, handleSubmit }) => {
  if (me) {
    return <Redirect to='/' />
  }
  return (
    <Layout style={{ padding: '30px 16px 0' }}>
      <Row
        type='flex'
        justify='center'
        align='middle'
        style={{ marginTop: -100 }}
      >
        <Col xs={24} md={12} lg={8} xl={6}>
          <Layout.Content
            style={{
              background: '#fff',
              padding: '3rem 3rem 1rem'
            }}
          >
            <h1>Выполните вход</h1>
            <Form onSubmit={handleSubmit} className='login-form'>
              <Form.Item>
                {form.getFieldDecorator('email', {
                  rules: [{
                    required: true,
                    message: 'Пожалуйста введите email!'
                  }]
                })(
                  <Input
                    prefix={
                      <Icon
                        type='user'
                        style={{ color: 'rgba(0,0,0,.25)' }}
                      />
                    }
                    placeholder='Электронная почта'
                  />
                )}
              </Form.Item>
              <Form.Item>
                {form.getFieldDecorator('password', {
                  rules: [{
                    required: true,
                    message: 'Пожалуйста введите пароль!'
                  }]
                })(
                  <Input
                    prefix={
                      <Icon
                        type='lock'
                        style={{ color: 'rgba(0,0,0,.25)' }}
                      />
                    }
                    type='password'
                    placeholder='Пароль'
                  />
                )}
              </Form.Item>
              <Form.Item>
                <Button
                  type='primary'
                  htmlType='submit'
                  className='login-form-button'
                  icon='login'
                >
                  Войти
                </Button>
              </Form.Item>
            </Form>
          </Layout.Content>
        </Col>
      </Row>
    </Layout>
  )
}

Login.propTypes = {
  me: PropTypes.object,
  client: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired
}

const enhance = compose(
  withApollo,
  graphql(LOGIN_MUTATE, { name: 'login' }),
  Form.create(),
  withHandlers({
    handleSubmit: ({ form, login, resetStore }) => (event) => {
      event.preventDefault()
      form.validateFields((errors, values) => {
        if (errors) {
          forEach(errors, field => forEach(field.errors, error => message.error(error.message)))
        } else {
          login({ variables: values })
            .then(({ data }) => localforage.setItem('jwt', data.loginAdmin.jwt))
            .then(resetStore)
            .then(() => message.success('Вы успешно выполнили вход!'))
            .catch(error => message.error(error.message))
        }
      })
    }
  })
)

export default enhance(Login)
