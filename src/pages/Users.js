/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import gql from 'graphql-tag'
import { Query, Mutation } from 'react-apollo'
import { Row, Col, Divider, Table, Popconfirm, Button, message, Spin } from 'antd'
import { withStateHandlers } from 'recompose'

import { filter } from 'lodash'

const USERS_QUERY = gql`
  query Users(
    $sortOrder: SortOrderEnum
    $sortField: UsersSortFieldEnum
    $filters: UsersFilterFields
    $limit: Int = 4
    $skip: Int
  ) {
    users(
      sortField: $sortField
      sortOrder: $sortOrder
      filters: $filters
      limit: $limit
      skip: $skip
    ) {
      count
      items {
        _id
        role
        name
        email
        createdAt(format: "DD.MM.YYYY, HH:mm")
      }
    }
  }
`

const REMOVE_USER_MUTATION = gql`
  mutation RemoveUser($_id: ID!) {
    removeUser(_id: $_id) {
      _id
      name
    }
  }
`

const Users = ({ variables, setVariables }) =>
  <React.Fragment>
    <Row>
      <Col><h1>Пользователи</h1></Col>
    </Row>
    <Divider />
    <Row style={{ marginTop: '8px' }}>
      <Col>
        <Query
          query={USERS_QUERY}
          fetchPolicy='cache-and-network'
          variables={variables}
        >
          {({ data, loading, fetchMore }) => {
            if (loading && !data.users) {
              return <Spin />
            }
            return (
              <React.Fragment>
                <Table
                  bordered
                  loading={loading}
                  onChange={setVariables}
                  rowKey='_id'
                  dataSource={data.users.items}
                  pagination={false}
                  columns={[
                    {
                      title: 'Имя',
                      sorter: true,
                      key: 'name',
                      dataIndex: 'name'
                    },
                    {
                      title: 'Email',
                      key: 'email',
                      dataIndex: 'email'
                    },
                    {
                      title: 'Роль',
                      filters: [
                        { text: 'Администратор', value: 'admin' },
                        { text: 'Клиент', value: 'client' }
                      ],
                      key: 'role',
                      dataIndex: 'role'
                    },
                    {
                      title: 'Зарегистрирован',
                      sorter: true,
                      key: 'createdAt',
                      dataIndex: 'createdAt'
                    },
                    {
                      title: '',
                      key: 'x',
                      width: 40,
                      dataIndex: '',
                      render: (text, { name, _id }) =>
                        <Mutation
                          mutation={REMOVE_USER_MUTATION}
                          variables={{ _id }}
                          onError={(error) => message.error(error.message)}
                          update={(store, { data: { removeUser } }) => {
                            /**
                             * Get current store from query
                             */
                            const data = store.readQuery({ query: USERS_QUERY, variables })

                            /**
                             * Remove deleted user from locale store
                             */
                            data.users.count -= 1
                            data.users.items = filter(data.users.items, (user) =>
                              user._id !== removeUser._id
                            )

                            /**
                             * Write new data wihtout deleted user
                             */
                            store.writeQuery({ query: USERS_QUERY, data, variables })

                            /**
                             * Show success message
                             */
                            message.success(`Пользователь "${removeUser.name}" был удален`)
                          }}
                        >
                          {(removeUser) =>
                            <Popconfirm
                              placement='left'
                              title={`Вы действительно хотите удалить пользователя ${name}?`}
                              onConfirm={removeUser}
                              okText='Да'
                              cancelText='Нет'
                            >
                              <Button type='danger' icon='delete' />
                            </Popconfirm>
                          }
                        </Mutation>
                    }
                  ]}
                />
                {data.users.count > data.users.items.length &&
                  <Button
                    style={{ marginTop: 10 }}
                    icon='plus'
                    onClick={() =>
                      fetchMore({
                        variables: {
                          ...variables,
                          skip: data.users.items.length
                        },
                        updateQuery: (prev, { fetchMoreResult }) => {
                          if (!fetchMoreResult) {
                            return prev
                          }
                          return {
                            ...prev,
                            users: {
                              ...prev.users,
                              items: [
                                ...prev.users.items,
                                ...fetchMoreResult.users.items
                              ]
                            }
                          }
                        }
                      })
                    }
                  >
                    Загрузить еще
                  </Button>
                }
              </React.Fragment>
            )
          }}
        </Query>
      </Col>
    </Row>
  </React.Fragment>

Users.propTypes = {
  variables: PropTypes.object.isRequired,
  setVariables: PropTypes.func.isRequired
}

const enhance = withStateHandlers(
  {
    variables: {}
  },
  {
    setVariables: ({ variables }) => (pagination, filters, sorter) => ({
      variables: {
        ...variables,
        filters,
        sortField: sorter.field,
        sortOrder: sorter.order
      }
    })
  }
)

export default enhance(Users)
