/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import localforage from 'localforage'
import { compose, withStateHandlers, withHandlers } from 'recompose'
import { Redirect, Switch, Route, withRouter } from 'react-router-dom'
import { Layout, Menu, Icon, Button, Row, Col, Popconfirm, message } from 'antd'
import { Users, Contests } from 'pages'

const { Header, Sider, Content } = Layout

const HomePure = ({ collapsed, me, signOut, onCollapse, history }) => {
  if (!me) {
    return <Redirect to='/login' />
  }
  return (
    <Layout>
      <Sider
        collapsible
        collapsed={collapsed}
        onCollapse={onCollapse}
        breakpoint='md'
        width={240}
      >
        <div className='logo' style={{ fontSize: 32 }} />
        <Menu
          theme='dark'
          mode='inline'
          onClick={({ key }) => history.push(key)}
          selectable={false}
        >
          <Menu.Item key='/'>
            <Icon type='folder-open' />
            <span className='users'>Пользователи</span>
          </Menu.Item>
          <Menu.Item key='contests'>
            <Icon type='folder-add' />
            <span className='nav-text'>Конкурсы</span>
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout>
        <Header style={{ background: '#fff', padding: '0 24px' }}>
          <Row type='flex' justify='space-between' align='middle'>
            <Col span={6}>
              <Icon
                className='trigger'
                type={collapsed ? 'menu-unfold' : 'menu-fold'}
                onClick={() => onCollapse(!collapsed)}
                style={{ cursor: 'pointer' }}
              />
            </Col>
            <Col>
              <Popconfirm
                placement='bottomLeft'
                title='Вы действительно хотите выйти?'
                onConfirm={signOut}
                okText='Да'
                cancelText='Нет'
              >
                <Button icon='logout' />
              </Popconfirm>
            </Col>
          </Row>
        </Header>
        <Content style={{ margin: '24px 16px 0', background: '#fff', padding: '1.5rem' }}>
          <Switch>
            <Route path='/' component={Users} exact />
            <Route path='/contests' component={Contests} />
            <Redirect to='/' />
          </Switch>
        </Content>
      </Layout>
    </Layout>
  )
}

HomePure.propTypes = {
  collapsed: PropTypes.bool.isRequired,
  signOut: PropTypes.func.isRequired,
  onCollapse: PropTypes.func.isRequired,
  me: PropTypes.object,
  history: PropTypes.object.isRequired
}

const enhance = compose(
  withRouter,
  withStateHandlers(
    {
      collapsed: false
    },
    {
      onCollapse: () => (collapsed) => ({ collapsed })
    }
  ),
  withHandlers({
    signOut: ({ resetStore }) => () => {
      localforage.removeItem('jwt')
        .then(resetStore)
        .then(() => message.success('Вы успешно вышли. До скорой встречи!'))
    }
  })
)

export default enhance(HomePure)
