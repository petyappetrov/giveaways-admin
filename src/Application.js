/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Route, Switch } from 'react-router-dom'
import { hot } from 'react-hot-loader'
import { Spin } from 'antd'
import { Query, withApollo } from 'react-apollo'
import gql from 'graphql-tag'
import { compose, withState, withHandlers } from 'recompose'
import { Home, Login } from 'pages'

import 'antd/dist/antd.css'
import './theme/styles.css'

const ME_QUERY = gql`
  query Me {
    me {
      _id
      name
      email
    }
  }
`

const Main = ({ resetStore, isReseting }) =>
  <Query
    query={ME_QUERY}
    skip={isReseting}
  >
    {({ loading, data = {} }) => {
      if (loading) {
        return <Spin />
      }
      return (
        <Switch>
          <Route
            path='/login'
            render={(props) =>
              <Login {...props} resetStore={resetStore} me={data.me} />
            }
          />
          <Route
            path='/'
            render={(props) =>
              <Home {...props} resetStore={resetStore} me={data.me} />
            }
          />
        </Switch>
      )
    }}
  </Query>

Main.propTypes = {
  resetStore: PropTypes.func.isRequired,
  isReseting: PropTypes.bool.isRequired
}

const enhance = compose(
  withApollo,
  withState('isReseting', 'setResetingState', false),
  withHandlers({
    resetStore: ({ setResetingState, client }) => () => {
      /**
       * This need to skipping of requests
       */
      setResetingState(true, () =>
        client.resetStore().then(() => setResetingState(false))
      )
    }
  })
)

export default hot(module)(enhance(Main))
