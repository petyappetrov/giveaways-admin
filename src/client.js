/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import { render } from 'react-dom'
import { BrowserRouter as Router } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo'
import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context'
import { InMemoryCache } from 'apollo-cache-inmemory'
import localforage from 'localforage'
import Application from './Application'

const httpLink = createHttpLink({
  uri: process.env.GRAPHQL_URI
})

const authLink = setContext((root, { headers }) =>
  localforage.getItem('jwt').then(token => ({
    headers: {
      ...headers,
      Authorization: token ? `Bearer ${token}` : ''
    }
  }))
)

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache(),
  connectToDevTools: true
})

render(
  <ApolloProvider client={client}>
    <Router>
      <Application />
    </Router>
  </ApolloProvider>,
  document.getElementById('root')
)
