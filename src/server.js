/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

const Koa = require('koa')
const webpack = require('webpack')
const koaWebpack = require('koa-webpack')
const path = require('path')
const koaStatic = require('koa-static')
const historyApiFallback = require('koa-connect-history-api-fallback')
const config = require('./config')
const webpackConfig = require('../webpack/config.dev')

const app = new Koa()

app
  .use(koaStatic(path.join(__dirname, '../dist')))
  .use(historyApiFallback({ verbose: false }))

if (process.env.NODE_ENV === 'development') {
  const compiler = webpack(webpackConfig)
  koaWebpack({ compiler }).then(middleware => app.use(middleware))
}

app.listen(config.port, (err) => {
  if (err) {
    throw new Error(err)
  }
  console.log(`Server started on http://localhost:${config.port}`)
})
