/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

const webpack = require('webpack')
const path = require('path')
const HTMLHarddiskPlugin = require('html-webpack-harddisk-plugin')
const HTMLPlugin = require('html-webpack-plugin')
const HTMLTemplate = require('html-webpack-template')

module.exports = {
  output: {
    filename: 'js/app-[hash].js',
    path: path.resolve(__dirname, '../dist'),
    publicPath: '/'
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: [
          'babel-loader'
        ],
        exclude: /node_modules/
      },
      {
        test: /\.(graphql|gql)$/,
        exclude: /node_modules/,
        loader: 'graphql-tag/loader'
      },
      {
        test: /\.(png|jpg|jpeg|gif)$/,
        use: [
          {
            loader: 'file-loader?name=img/[name]-[hash].[ext]'
          }
        ]
      }
    ]
  },

  plugins: [
    new webpack.EnvironmentPlugin([
      'GRAPHQL_URI'
    ]),
    new HTMLPlugin({
      inject: false,
      template: HTMLTemplate,
      appMountId: 'root',
      alwaysWriteToDisk: true,
      mobile: true,
      lang: 'ru'
    }),
    new HTMLHarddiskPlugin()
  ],

  resolve: {
    extensions: ['.js', '.json'],
    modules: [
      'node_modules',
      path.join(__dirname, '../src')
    ]
  }
}
